<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Detail Data</title>

</head>

<body>

<h2>Detail Data Game</h2>

<div class="card-body">
    <div class="input-group mb-3">
        <span class="input-group-text" id="id">Id Game</span>
        <input type="text" class="form-control" placeholder="{{$detail->id}}" aria-label="id" aria-describedby="id" readonly>
    </div>
    <div class="input-group mb-3">
        <span class="input-group-text" id="name">Nama Game</span>
        <input type="text" class="form-control" placeholder="{{$detail->name}}" aria-label="Name" aria-describedby="name" readonly>
    </div>
    <div class="input-group mb-3">
        <span class="input-group-text" id="gameplay">Gameplay Game</span>
        <input type="text" class="form-control" placeholder="{{$detail->gameplay}}" aria-label="Gameplay" aria-describedby="gameplay" readonly>
    </div>
    <div class="input-group mb-3">
        <span class="input-group-text" id="developer">Developer Game</span>
        <input type="text" class="form-control" placeholder="{{$detail->developer}}" aria-label="Developer" aria-describedby="developer" readonly>
    </div>
    <div class="input-group mb-3">
        <span class="input-group-text" id="year">Tahun Game</span>
        <input type="text" class="form-control" placeholder="{{$detail->year}}" aria-label="Year" aria-describedby="year" readonly>
    </div>
    <div class="input-group mb-3">
        <a href="/game" class="btn btn-danger">Back</a>
    </div>
</div>




<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>