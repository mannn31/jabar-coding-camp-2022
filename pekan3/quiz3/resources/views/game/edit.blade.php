<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Edit Data</title>

</head>

<body>

<h2>Edit Data Game</h2>

<form role="form" action="/game/{{ $edit->id }}" method="POST">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="mb-3">
            <label for="name" class="form-label">Nama Game</label>
            <input type="text" class="form-control" id="name" value="{{ old('name', $edit->name) }}">
        </div>
        <div class="mb-3">
            <label for="gameplay" class="form-label">Gameplay</label>
            <textarea class="form-control" id="gameplay" rows="3">{{ old('gameplay', $edit->gameplay) }}</textarea>
        </div>
        <div class="mb-3">
            <label for="developer" class="form-label">Nama Developer</label>
            <input type="text" class="form-control" id="developer" value="{{ old('developer', $edit->developer) }}">
        </div>
        <div class="mb-3">
            <label for="year" class="form-label">Tahun Rilis</label>
            <input type="text" class="form-control" id="year" value="{{ old('year', $edit->year) }}">
        </div>
        <div class="mb-3">
            <button type="submit" class="btn btn-primary">Edit</button>
            <a href="/game" class="btn btn-danger">Back</a>
        </div>
    </div>
</form>




<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>