<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class GameController extends Controller
{
    public function index()
    {
        $game = DB::table('game')->get();
        return view('game.index', compact('game'));
    }

    public function show($id)
    {
        $detail = DB::table('game')->where('id', $id)->first();
        return view('game.show', compact('detail'));
    }
    
    public function create()
    {
        return view('game.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);
        $query = DB::table('game')->insert([
            "name" => $request["name"],
            "gameplay" => $request["gameplay"],
            "developer" => $request["developer"],
            "year" => $request["year"]
        ]);
        return redirect('/game');
    }

    public function edit($id)
    {
        $edit = DB::table('game')->where('id', $id)->first();
        return view('game.edit', compact('edit'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ]);

        $query = DB::table('game')
            ->where('id', $id)
            ->update([
                "name" => $request["name"],
                "gameplay" => $request["gameplay"],
                "developer" => $request["developer"],
                "year" => $request["year"]
            ]);
        return redirect('/game');
    }
    
    public function destroy($id)
    {
        $query = DB::table('game')->where('id', $id)->delete();
        return redirect('/game');
    }

}
