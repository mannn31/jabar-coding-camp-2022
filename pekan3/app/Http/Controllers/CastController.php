<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CastController extends Controller
{
    public function index(){
        return view('cast.index');
    }

    public function create(){
        return view('cast.create');
    }
}
