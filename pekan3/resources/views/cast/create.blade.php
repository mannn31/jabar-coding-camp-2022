@extends('main')

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Create New Cast</h3>
    </div>

    <form>
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama Cast</label>
                <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama Lengkap">
            </div>
            <div class="form-group">
                <label for="umur">Umur Cast</label>
                <input type="number" class="form-control" id="umur" placeholder="Masukkan Umur">
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" id="bio" placeholder="Masukkan Bio">
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection