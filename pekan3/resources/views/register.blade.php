<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>
            JCC MHS | Hari 12
        </title>
    </head>
<body>

<h1>Buat Account Baru</h1>
<h2>Sign Up Form</h2>
<form action="welcome" method="POST">
    @csrf
    <label>First Name :</label><br><br>
    <input type="text" name="fname"><br><br>
    <label>Last Name :</label><br><br>
    <input type="text" name="lname"><br><br>
    <label>Gender</label><br><br>
    <input type="radio" name="gender">Man<br>
    <input type="radio" name="gender">Woman<br>
    <input type="radio" name="gender">Other<br><br>
    <label>Nationality</label><br><br>
    <select name="national">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Thailand">Thailand</option>
        <option value="Australia">Australia</option>
    </select><br><br>
    <label>Language Spoken</label><br><br>
    <input type="checkbox" name="language">Bahasa Indonesia<br>
    <input type="checkbox" name="language">English<br>
    <input type="checkbox" name="language">Arabic<br>
    <input type="checkbox" name="language">Japanese<br><br>
    <label>Bio</label><br><br>
    <textarea name="bio" cols="30" rows="10"></textarea><br><br>
    <input type="submit" value="Sign Up">
</form>

</body>
</html>